package Games;

import userlogic.InformationAboutUser;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by Maksim on 26.10.15.
 * пользователь выбирает тот или иной вариант и играет - в первом случае - он вводит количество выкидываний монетки
 * и ставку на тот или иной вариант - например решка, 200 долларов, 10 закидываний. Программа проганяет все варианты,
 * каджый из них выводит на экран - например "выпала решка, совпало, ставка + 200 долларов, общая сумма - 1200
 * (например, это зависит от стартовой суммы у пользователя)", "выпал орел - несовпало, - 200, общая сумма - 800".
 * В конце выводит общую сумму, которая осталась.
 */
public class OrelReshka {

    public static int currentMoney;
    public static String side;
    public static String textResult;

    public static void orelReshkaGame() {
        Scanner scanner = new Scanner(System.in);

        int gameMoneyResult = InformationAboutUser.startMoney;


        Random random = new Random();

        System.out.println("Сколько раз выкидывать монетку? ");
        int coinTry = scanner.nextInt();

        System.out.println("На какую сторону ставите? Орел или Решка:");
        String coinSide = scanner.next();  //changed   r

        System.out.println("Какой размер ставки в этой игре:");
        int bid = scanner.nextInt();

        System.out.println("Играем!!!");


        for (int i = 0; i <= coinTry; i++) {
            int sideResult = random.nextInt(2);


            if (sideResult == 0 && coinSide.equalsIgnoreCase("Решка")) {
                // Ноль это решка
                side = "Решка";
                gameMoneyResult = gameMoneyResult + bid;
                textResult = "Совпало";
                System.out.println("Выпала " + side + " " + textResult + " ваш текущий баланс " + gameMoneyResult);


            } else if (sideResult == 1 && coinSide.equalsIgnoreCase("Орел")) {
                side = "Орел";
                // 1 значит орел
                gameMoneyResult = gameMoneyResult + bid;
                textResult = "Совпало";
                System.out.println("Выпала " + side + " " + textResult + " ваш текущий баланс " + gameMoneyResult);

            } else if (sideResult == 0 || sideResult == 1) {
                side = "Решка";
                textResult = "не совпало";
                gameMoneyResult = gameMoneyResult - bid;
                System.out.println("Выпала " + side + " " + textResult + " ваш текущий баланс " + gameMoneyResult);
            }

        }

    }
}
