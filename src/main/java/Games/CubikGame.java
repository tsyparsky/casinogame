package Games;

import userlogic.InformationAboutUser;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by Maksim on 26.10.15.
 * В случае с выбором второй игры - игральный кубик,
 * пользователь так-же вводит число от 1-6 и количество запусков + ставку на выиграш, так-же с каждым закидыванием кубика
 * программа выводит число, которое выпало, общую сумму и текущую ставку, в конце выводит общее количесво денег.
 */
public class CubikGame {
    public static void cubikGame() {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();

        int[] cubikSides = {1, 2, 3, 4, 5, 6};

        System.out.println("Пожалуйста введите число от 1 до 6 :");
        int cubikSideNumber = scan.nextInt();
        System.out.println("Сколько раз будем бросать кубик? Введите число:");
        int tryNumber = scan.nextInt();
        System.out.println("Введит вашу ставку в этой игре:");
        int bid = scan.nextInt();


        for (int i = 0; i <= tryNumber; i++) {

            int randomResult = random.nextInt(6);

            if (cubikSides[randomResult] == tryNumber) {
                InformationAboutUser.startMoney = InformationAboutUser.startMoney + bid;

                System.out.println("Выпало число: " + cubikSides[randomResult] + " " + InformationAboutUser.userName + " ваша ставка была на число: " + cubikSideNumber + " ваш баланс " + InformationAboutUser.startMoney + " ваша текущая ставка " + bid);

            } else if (cubikSides[randomResult] != tryNumber) {

                InformationAboutUser.startMoney = InformationAboutUser.startMoney - bid;
                System.out.println("Выпало число: " + cubikSides[randomResult] + " " + InformationAboutUser.userName + " ваша ставка была на число: " + cubikSideNumber + " ваш баланс " + InformationAboutUser.startMoney + " ваша текущая ставка " + bid);

            }
        }

        System.out.println("Ваш баланс после текущей игры: " + InformationAboutUser.startMoney);


    }
}

