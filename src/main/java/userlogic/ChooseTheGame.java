package userlogic;

import Games.CubikGame;
import Games.OrelReshka;

import java.util.Scanner;

/*
 * Created by Maksim on 26.10.15.
 */
public class ChooseTheGame {

    public static void gameToPlay() {
        Scanner scan = new Scanner(System.in);
        System.out.println("У нас для, Вас на выбор две игры: ОрелРешка и Кубики. Введите название игры в которую хотите играть:");
        String gameName = scan.nextLine();

        String gameNameValidation = gameName.toLowerCase();

        if (gameName.equalsIgnoreCase("ОрелРешка")) {
            System.out.println("Хороший выбор! Играем в \"ОрелРешка\"");
            OrelReshka.orelReshkaGame();

        } else if (gameName.equalsIgnoreCase("Кубики")) {

            System.out.println("Хороший выбор! Играем в \"Кубики\"");
            CubikGame.cubikGame();
        }
    }

}

